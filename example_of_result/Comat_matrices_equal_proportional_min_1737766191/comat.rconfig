#########################################################################
##                                                                     ##
##     Comat rconfig                                                   ##
##                                                                     ##
##     Gael A. Millot                                                  ##
##     Bioinformatics and Biostatistics Hub                            ##
##     Computational Biology Department                                ##
##     Institut Pasteur Paris                                          ##
##                                                                     ##
#########################################################################




################################ Parameters that need to be set by the user


################ Mandatory settings


######## File names and locations


project.name <- "Comat"
file.name1 <- "all" # name of the data file 1. Can be (1) "all", or (2) a character vector of length n with the name of the files, or (3) a n x 2 character matrix giving on each line the couple of matrices to compare. If file.name1 = "all", then file.name2 must be "" and the 2 x 2 possible comparison is made on all the files present in path.in.If file.name1 is a character vector of length n, and file.name2 <- "" (see below), then n > 1 and every 2 x 2 possible comparison will be performed using file.name1. If file.name1 is a character vector of length n, and file.name2 != "", then file.name2 must be a character vector of length n
file.name2 <- "" # name of the data file 2. Not used if file.name1 is a n x 2 character matrix
path.in <- "C:/Users/gmillot/Documents/Git_projects/comat/dataset/data2/" # Define the absolute pathway of the folder containing the input data files (file.name1, name.proba, and name.quantile above). Use path.in <- "C:/Users/gmillot/Documents/Git_projects/comat/data2/" for debugging or path.in <- "C:/Users/gmillot/Documents/Hub projects/20170704 Pierre Bruhns 8528/Comat-v1.0 Analysis/VHVL comparison 2/" or path.in <- "C:/Users/gmillot/Documents/Hub projects/20190306 Pierre Bruhns 12401/Second_analysis/dataset/"
file.kind <- "txt" # kind of file to open. Either "txt" for .txt files, or "tsv" for tsv files, or "jpeg" for jpeg pictures
path.out <- "C:/Users/gmillot/Desktop/" # Define the absolute pathway of the destination folder that will contain the results (exported data)


######## Distance computation


user.model <- "sum(abs(matrix1 / sum(matrix1, na.rm = TRUE) - matrix2 / sum(matrix2, na.rm = TRUE)))/2"
# single character string indicating the formula that computes the distance between the two matrices (for p value and for MDS)
# BEWARE: the only variables must be "matrix1" and "matrix2". For instance, it can be Hellinger: (sum(((matrix1 / n1)^0.5 - (matrix2 / n2)^0.5)^2))^0.5 * 1/(2)^0.5, or sum(abs(matrix1 / n1 - matrix2 / n2))/2 (has the advantage to be very similar to Hellinger distance but with variation between 0 and 1)
# BEWARE: write the model such that NaN (resulting from 0/0 for instance) and NA are removed. For instance, use sum(, na.rm = TRUE) Any computed VT equal to NA will generate an error
user.model.by.cell <- "abs(matrix1 / sum(matrix1, na.rm = TRUE) - matrix2 / sum(matrix2, na.rm = TRUE))/2" # BEWARE: must be the same as user.model but keeping the results by cell (keeping a matrix of same dimension as matrix 1 and matrix 2)
kind.of.comparison <- "matrices" # compare either "matrices" (full matrices comparison), or "row.sums" or "column.sums"


######## Multi Dimensional Scale (MDS) analysis


mds.analysis <- TRUE # perform MDS ? BEWARE: only possible if the user.model parameters computes positive values only
class.mds <- c("TT", "GPI", "TSPAN8") # if some of the compared matrices belong to a same group, referenced by a common character string in the matrix names, these common charater strings can be indicated. Then, on the MDS display, matrices belonging to a same group will have the same dot color. Example: c("TT", "GPI", "TSPAN8"). Write "" if not relevant



################ End Mandatory settings


################ Optional settings


######## File names and locations


lib.path <- NULL # vector of character strings that define the absolute pathway of the folder containing the R packages. Write NULL for the default path. BEWARE: default path is dependent on the system and interface used. For instance, using cygwin64 on windows 7, the path is "C:/Program Files/R/R-3.5.3/library". On the same cmputer using the R classical interface, the paths are [1] "C:/Users/gmillot/Documents/R/win-library/3.5" [2] "C:/Program Files/R/R-3.5.3/library"
path.function1 <- "https://gitlab.pasteur.fr/gmillot/cute_little_R_functions/-/raw/v11.6/cute_little_R_functions.R" # Define the absolute pathway of the folder containing functions created by Gael Millot
path.function2 <- "C:/Users/gmillot/Documents/Git_projects/comat/other_comat_functions.R" # Define the absolute pathway of the folder containing functions created by Gael Millot


######## Matrix modification


empty.rm <- TRUE # logical. Remove the common empty rows and columns before analysis ? Either TRUE or FALSE
readjust <- "equal_proportional_min" # can be c("NO", "row&col_iterations", "equal_proportional_mean", "equal_proportional_min", "equal_by_subtraction", "equal_by_sampling_rm", "max_low", "max_up"). BEWARE: except "NO" and "equal_by_subtraction", no integer matrices anymore because of averaging -> fixed.var parameter cannot be used. To add other methods: see also https://en.wikipedia.org/wiki/Histogram_equalization, https://en.wikipedia.org/wiki/Normalization_(image_processing)
# "NO": nothing is performed
# "row&col_iterations": each matrix is normalized  such that every row sum and col sum have the same weight -> iterative method that divides rows by sum(rows) and cols by sum(cols). The row.col.iter.nb parameter is required. BEWARE: the final size n1 and n2 of each matrix will be nrow+ncol
# "equal_proportional_mean": perform a readjustment of the matrice size such as the two matrice have the same size n = mean(n1, n2). This is to test that significant difference is not due to very unbalanced matrices. For instance matrix1 with n1 = 1 (very poor sequencing) and matrix2 n2 = 2000. The comparison will be very significant but is is just because of the poor quality of matrix1
# "equal_proportional_min": perform a readjustment of the matrice size such as the highest matrice has the same size n than the lowest(the computing is to multiply each cell by sum(matrix1) / sum(matrix2) if matrix2 has the highest n). Beware: information is lost because one of the matrix is degraded. In this method, the null cells remain null and no new null cells are created. This is to test that significant difference is not due to very unbalanced matrices. For instance matrix1 with n1 = 1 (very poor sequencing) and matrix2 n2 = 2000. The comparison will be very significant but is is just because of the poor quality of matrix1
# "equal_by_subtraction": perform a readjustment such as the highest matrice has the same size n than the lowest, by substracting all the cell by readjust.approx and by setting all the negative cell to 0. The readjust.approx parameter is required. Beware: information is lost because one of the matrix is degraded. In this method, new null cells can be created
# "equal_by_sampling_rm": perform a readjustment such as the highest matrice with n1 size has finally the same n2 size than the lowest matrix, by randomly removing n1-n2 individuals. This is performed k times and a mean matrix is computed. Beware: information is lost because one of the matrix is degraded. In this method, new null cells can be created. # BEWARE: deal with non integer matrices (i.e., when using the pre.readjust parameter) for the sampling the final matrix obtained must have exactly the same size as the non modified matrix
# "max_low": reduce the matrix with the highest cell value such as it has the same highest cell value than the other matrix (the computing is to multiply each cell by max(matrix1) / max(matrix2) if matrix2 has the highest max value). The size n will not be the same in such case, only max(matrix) will be the same
# "max_up": increase the matrix with the highest cell value such as it has the same highest cell value than the other matrix (the computing is to multiply each cell by max(matrix2) / max(matrix1) if matrix2 has the highest max value)
readjust.approx <- 1/1000 # single proportion indicating the substracting value when readjust parameter is "equal_by_subtraction". Ignored for the other readjust settings
rm.sampling.nb <- 1e4 # positive integer value indicating the number of samplings when readjust parameter is "equal_by_sampling_rm". Ignored for the other readjust settings
loop.message.rm.sampling.nb <- rm.sampling.nb/100 #  positive integer value indicating after which number a message as to be displayed when the number of samplings when readjust parameter is "equal_by_sampling_rm". Must not be over rm.sampling.nb. Ignored for the other readjust settings
row.col.iter.nb <- 10 # number of iteration for the "row&col_iterations" option of the readjust parameter
save.iteration <- TRUE # logical. Save the iteration display data in .RData file? Ignored except if readjust parameter is "row&col_iterations"
quantile.iter <- c(0, 0.025, 0.25, 0.5, 0.75, 0.975, 1) # numeric vector of proportions indicating the quantiles for iteration display. Must be of odd length with 0.5 as median number. Ignored except if readjust parameter is "row&col_iterations"
pears <- FALSE # do you also want a pearson correlation test if readjust <-"equal_by_subtraction" or "equal_by_sampling_rm"?
p.value.pears <- "BH" # correction of p values due to multiple comparisons. One of these c("holm", "hochberg", "hommel", "bonferroni", "BH", "BY", "fdr", "none"), "BH" and "fdr" are the same


######## P value computation


randomization.kind <- "contingency" # kind of randomization performed. Either "contingency" (default) or "cell".
# "contingency": means that the value in each cell is a contingency. Thus, the value is a sum of sampling unit (for instance, a value 50 in a cell means that it has been observed that 50 individuals falls into that cell). In this situation, the randomization is n individuals (the size of the matrix) randomly thrown into the matrix.
# "cell": means that the value in each cell cannot be splitted into sampling unit. Thus, the sampling unit is the cell. In this situation, the randomization is kxc cells (the dimension of the matrix) randomly redistributed inside the matrix (cell positions are shuffled). This should be equivalent to the spearman test randomization
fixed.var <- "3" # fix some dimensions? One of this character string c("NO", "1", "2", "3", "12", "13", "23", "mean"). The choice will affect the way the sampling of the n1 and n2 individuals is performed to generate the p value. Ignored if randomization.kind argument is "cell"
# "NO": no fixed dimension. The sampling probability to get xijkz are all the same (1/(n1 + n2) or 1/n....), either in matrix 1 or matrix2 (no separation between matrix 1 and matrix 2, see "3" for the separation). This means that it is possible to have n.... = n in a single cell among the two matrices (all other cells being 0)
# "1": values of row margins are fixed during sampling (dimension 1). The sampling probability to get xijkz is 1/ni... at row level and 1/ni.../r at matrices level (r = number of rows). If 200 individuals in the 1st row of matrix 1 + matrix 2 (x1... = 200), then these 200 individuals are reshuffled in all the cells of row 1 of matrix 1 + matrix 2. This means that it is possible to have ni... in a single cell among the row i of the two matrices (all other cells of row i being 0)
# "2": values of column margins are fixed during sampling (dimension 2). The sampling probability to get xijkz is 1/n.j.. at column level and 1/n.j../c at matrices level (c = number of columns). If 200 individuals in the 1st column of matrix 1 + matrix 2 (n.1.. = 200), then these 200 individuals are reshuffled in all the cells of column 1 of matrix 1 + matrix 2. This means that it is possible to have n.j.. in a single cell among the column j of the two matrices (all other cells of column j being 0)
# "3": values of matrix sums (n1 and n2) are fixed during sampling (dimension 3). The sampling probability to get xijkz is 1/n..k., i.e., 1/n1 for matrix 1 and 1/n2 for matrix 2, and 1/n..k./2 at at matrices level. If 200 individuals in matrix 1 (n..1. = n1 = 200), then these 200 individuals are reshuffled in all the cells of matrix 1. This means that it is possible to have n..k. = nk in a single cell of matrix k (all other cells of matrix k being 0)
# "12": values of row and column margins are fixed during sampling (dimension 1 and 2). The sampling probability to get xijkz is 1/nij.. at row/colum level and 1/nij../(r * c) at matrices level. If 200 individuals in the 1st row and 1st column of matrix 1 + matrix 2 (x11.. = 200), then these 200 individuals are reshuffled in the 2 cells of row 1 column 1 of matrix 1 + matrix 2. This means that it is possible to have nij.. in a single cell among the 2 cells of the two matrices (the other cell being 0)
# "13": values of row margins of each matrix are fixed during sampling (dimension 1 and 3). The sampling probability to get xijkz is 1/ni.k. at row/matrix level and 1/ni.k./(r * 2) at matrices level. If 200 individuals in the 1st row and 1st matrix (x1.1. = 200), then these 200 individuals are reshuffled in the cells of row 1 of matrix 1. This means that it is possible to have ni.k. in a single cell among the cells of row i in matrix k (all other cells of row i and matrix k being 0)
# "23": values of column margins of each matrix are fixed during sampling (dimension 2 and 3). The sampling probability to get xijkz is 1/n.jk. at column/matrix level and 1/n.jk./(c * 2) at matrices level. If 200 individuals in the 1st column and 1st matrix (n.11. = 200), then these 200 individuals are reshuffled in the cells of column 1 of matrix 1. This means that it is possible to have n.jk. in a single cell among the cells of column j in matrix k (all other cells of column j and matrix k being 0)
# "mean": similar to "12" except that no margin are fixed. The sampling probability to get xijkz is the mean of nij1./n..1. and nij2./n..2.. If matrix1 and matrix2 are dim rc, then a mean matrix of dim rc is formed from matrix1 and matrix2, which is used as prob argument for the sample() function when generating the sampled matrix1 and also when generating the sampled matrix2. Thus, contrary to "12", the margins slightly fluctuate -> "mean" is in fact a sampling weight
sampling.nb <- 1e2 # Single positive and non null integer value fixing the number of sampling used for the VT distribution
show_theo <- TRUE # Single logical value. Display the first sampled theoretical matrix 1 and 2, according to the fixed.var parameter, before and after normalization, according to the readjust parameter
loop.message.dis.nb <- sampling.nb/100 # every loop.message.dis.nb, a message is displayed, that includes the expected job end. Must not be over sampling.nb


######## Multi Dimensional Scale (MDS) analysis


mds.confidence.int <- TRUE # draw the confidence interval. If ellipsoids/bars overlap, it means not significantly distant, otherwise significantly distant. BEWARE: require bootstrap which is long. Not used if mds.analysis == FALSE
mds.confidence.boundaries <- c(0, 1) # 2 quantiles for the confidence interval (2 values between 0 and 1)
mds.ci.quantile.type <- 7 # see ?quantile
mds.sampling.nb <- 1e2 # nb of bootstrap for mds.confidence.int
mds.loop.message.dis.nb <- mds.sampling.nb/100 # every mds.loop.message.dis.nb, a message is displayed, that includes the expected job end
mds.plot.dots <- TRUE # plot the dots used for CI ?
heatmap.clustering_method <- "ward.D2" # see ?hclust to see details on the different methods: "ward.D", "ward.D2", "single", "complete", "average" (= UPGMA), "mcquitty" (= WPGMA), "median" (= WPGMC) or "centroid" (= UPGMC)


######## Parallelization


parallelization <- TRUE # # Single logical value. Parallelize on local cpu ? Warning : if TRUE, sampling.nb parameter value must be over the number of cpu used
cpu_nb <- 0 # Single positive integer value fixing the number of cpu used for parallelization. Value 0 means "all cpu used"

######## Graphical and display parameters


#### parameters for the VT graphic


activate.pdf.VT <- TRUE # write TRUE for pdf display and FALSE for R display (main graphs)
display.threshold.VT <- 10000 # max number of point displayed
width.wind.VT <- 5 # window width (in inches)
height.wind.VT <- 5 # window height (in inches)
down.space.VT <- 0.6 # bottom margin space (in inches)
left.space.VT <- 0.5 # left margin space (in inches)
up.space.VT <- 0.2 # top margin space (in inches)
right.space.VT <- 0.2 # right margin space (in inches)
dist.legend.VT <- 0.5 # distance of axis legends (in inches)
amplif.VT <- 1.5 # increase or decrease the value to increase or decrease the size of the axis (numbers and legends)
amplif.corner.text.VT <- amplif.VT / 3
jit.VT <- 1 # randomly space the displayed data in stripcharts (unit of the x-axis)
pt.cex.strip.VT <- 1 # increase or decrease the value to increase or decrease the size of the points in stripcharts (between 0 and Inf)
x.nb.inter.tick.VT <- 0 # number of secondary ticks between main ticks (between 0 and Inf)
tick.length.VT <- 0.5 # length of the ticks (between 0 and 1). 0 means no ticks and 1 means that the ticks touch the axis scales (numbers)
sec.tick.length.VT <- 0.3 # length of the secondary ticks as tick.length.VT


#### parameters for the iteration (row_col_sum_norm) display


pdf.display.iter <- TRUE # evolution of sum of row and col during iterations displayed? write TRUE for pdf display and FALSE for R display
width.wind.iter <- 10 # window width (in inches)
height.wind.iter <- 5 # window height (in inches)
down.space.iter <- 3*0.2 # bottom margin space (in par(mar) units)
left.space.iter <- 3.5*0.2 # left margin space (in par(mar) units)
up.space.iter <- 4*0.2 # top margin space (in par(mar) units)
right.space.iter <- 1*0.2 # right margin space (in par(mar) units)
dist.legend.iter <- 2 # distance of axis legends (in par(mgp) units)
amplif.iter <- 1 # increase or decrease the value to increase or decrease the size of the axis (numbers and labels)
amplif.legend.iter <- 1 # increase or decrease the value to increase or decrease the size of the legend
amplif.corner.text.iter <- 0.5


#### parameters for the stat display


pdf.display.stat <- TRUE # summary stat on matrix displayed? write TRUE for pdf display and FALSE for R display
displayed.nb.stat <- 1e4 # number of values displayed. If NULL, all the values are displayed. Otherwise, displayed.nb values are randomly selected for the display
single.value.display.stat <- FALSE # provide the 4 graphs if data is made of a single  (potentially repeated value)? If FALSE, an empty graph is displayed if data is made of a single (potentially repeated value). And the return list is made of NULL compartments
cut.off.method.stat <- "" # Write "" if not required. write "mean.sd" if mean +/- sd are required (BEWARE: only for normal distribution). Write "quantile" if trimming is based on quantile cut-offs
cut.off.trim.interval.stat <- c(0.05, 0.975) # Write "" if not required. Use a couple of values c(lower, upper) indicating the lower and upper interval, which represent the interval of distribution kept (between 0 and 1). Example: trim.dist.f = c(0.05, 0.975). What is strictly kept is ]lower , upper[, boundaries excluded. Using the "mean.sd" method, 0.025 and 0.975 represent 95% CI which is mean +/- 1.96 * sd
width.wind.stat <- 7 # window width (in inches)
height.wind.stat <- 7 # window height (in inches)
down.space.stat <- 3*0.2 # bottom margin space (in par(mar) units)
left.space.stat <- 3.5*0.2 # left margin space (in par(mar) units)
up.space.stat <- 0.4 # top margin space (in par(mar) units)
right.space.stat <- 0.2 # right margin space (in par(mar) units)
dist.legend.stat <- 1.5 # distance of axis legends (in par(mgp) units)
amplif.stat <- 1 # increase or decrease the value to increase or decrease the size of the axis (numbers and labels)
cex.pt.stat <- 0.2 # size of points in stripcharts (in inches)
amplif.legend.stat <- 1 # increase or decrease the value to increase or decrease the size of the legend
amplif.corner.text.stat <- 0.5


#### parameters for the matrix colorization


# parameters for fun_open_window() and windows opening
activate.pdf.color.mat <- TRUE # write TRUE for pdf display and FALSE for R display (graphs related to the MWW tests used in the MWW method)
on.same.pdf <- TRUE # put the colored matrix on the same pdf ? BEWARE : if TRUE, the size page is the same in a pdf document. This can create problems if batch of matrice with different sizes. CANNOT BE TRUE IF activate.pdf.color.mat is FALSE
# parameters for fun.mat.fixed.window.size()
width.wind.mat.plot <- 8 # window width (in inches)
height.wind.mat.plot <- 8 # window height (in inches)
margin.space.mat.plot <- 0.75 # margins of the main plot (in inches)
scale.wind.width <- 2 # window scale width (in inches)
scale.margin.space <- 2 # margins of the scale plot (in inches)
scale.plot.region.width.multi <- 0.15 # multiplication factor of the scale plot region width relative to scale.wind.width. If equal to 1, scale.margin.space applies for the four sides. If different from 1, scale.margin.space applies for up and down margins only
# parameters for fun.matrix.rgb.colorization()
type.color.matrix <- "invert" # either black null reference ("std") or white null reference ("invert")
z.min.mat.plot <- NULL # add a minimal value to the dynamic scale of mat. Beware: this rescale the dynamic & gradient colors
z.max.mat.plot <- NULL # add a maximal  value to the dynamic scale of mat. Beware: this rescale the dynamic & gradient colors

# parameters for fun.colored.fixed.matrix.plot()
null.dyn.display <- TRUE # display something when no dynamic ?
disp.axis.mat.plot <- TRUE # display ticks and row and column names on axis ?
xside.mat.plot <- 3 # x-axis at the bottom (1) or top (3) of the region figure. Write 0 if no axis required
yside.mat.plot <- 2 # y-axis at the left (2) or right (4) of the region figure. Write 0 if no axis required
x.nb.inter.tick.mat.plot <- 0 # number of secondary ticks between main ticks on x-axis (only if not log scale). Zero means non secondary ticks
y.nb.inter.tick.mat.plot <- 0 # number of secondary ticks between main ticks on y-axis (only if not log scale). Zero means non secondary ticks
tick.length.mat.plot <- 0.5 # length of the ticks (1 means complete the distance between the plot region and the axis numbers, 0.5 means half the length, etc. O means no ticks
axis.amplif.mat.plot <- 0.5 # increase or decrease the value to increase or decrease the size of the axis numbers
orient.mat.plot <- 2 # for par(las)
dist.legend.mat.plot <- 2 # increase the number to move axis legends away
box.type.mat.plot <- "o" # kind of frame surrounding the plot region
std.x.range.mat.plot <- TRUE # standard range on the x-axis? TRUE (no range extend) or FALSE (4% range extend). Controls xaxs argument of par() (TRUE is xaxs = "i", FALSE is xaxs = "r")
std.y.range.mat.plot <- TRUE # standard range on the y-axis? TRUE (no range extend) or FALSE (4% range extend). Controls yaxs argument of par() (TRUE is yaxs = "i", FALSE is yaxs = "r")
amplif.corner.text.mat.plot <- 0.4
disp.axis.scale.mat.plot <- TRUE # display axis scale?
xside.scale.mat.plot <- 0 # x-axis at the bottom (1) or top (3) of the region figure. Write 0 if no axis required
yside.scale.mat.plot <- 4 # y-axis at the left (2) or right (4) of the region figure. Write 0 if no axis required
nb.main.tick.scale.mat.plot <- 3 # nb of main ticks and labels
nb.inter.tick.scale.mat.plot <- 0 # number of secondary ticks between main ticks (only if not log scale). Zero means non secondary ticks
tick.length.scale.mat.plot <- 0.5 # length of the ticks (1 means complete the distance between the plot region and the axis numbers, 0.5 means half the length, etc. O means no ticks
orient.scale.mat.plot <- 2 # for par(las)
dist.legend.scale.mat.plot <- 3.5 # increase the number to move axis legends away
box.type.scale.mat.plot <- "o" # kind of frame surrounding the plot region
axis.amplif.scale.mat.plot <- 1 # increase or decrease the value to increase or decrease the size of the axis numbers in the scale plot
label.amplif.scale.mat.plot <- 1 # increase or decrease the value to increase or decrease the size of the axis labels in the scale plot

# parameters specific to the matrix overlay
scale.margin.space.2D <- 0.5 # margins of the scale plot (in inches)
xside.scale.2D.mat.plot <- 1 # for the 2D scale. X-axis at the bottom (1) or top (3) of the region figure. Write 0 if no axis required
yside.scale.2D.mat.plot <- 2 # for the 2D scale. Y-axis at the left (2) or right (4) of the region figure. Write 0 if no axis required


#### parameters for the vizualisation of the model applied on each matrix cell


# parameters for fun_open_window() and windows opening
activate.diff <- TRUE # write TRUE for display and FALSE for no display
activate.pdf.diff <- TRUE # write TRUE for pdf display and FALSE for R display (graphs related to the MWW tests used in the MWW method)
on.same.pdf.diff <- TRUE # put the diff matrix on the same pdf than the colored matrices? BEWARE : if TRUE, the size page is the same in a pdf document. This can create problems if batch of matrice with different sizes. CANNOT BE TRUE IF activate.pdf.color.mat is FALSE
# parameters for fun.mat.fixed.window.size()
# use exactly the same parameters than for the main matrices (all the .plot parameters). From width.wind.mat.plot to label.amplif.scale.mat.plot, except the z.min.mat.plot and z.max.mat.plot parameters


#### parameters for the vizualisation of the proba matrix (sample function)


# parameters for fun_open_window() and windows opening
activate.prob <- TRUE # write TRUE for display and FALSE for no display. Ignored if fixed.var == "NO"
activate.pdf.prob <- TRUE # write TRUE for pdf display and FALSE for R display (graphs related to the MWW tests used in the MWW method)
on.same.pdf.prob <- TRUE # put the diff matrix on the same pdf than the colored matrices? BEWARE : if TRUE, the size page is the same in a pdf document. This can create problems if batch of matrice with different sizes. CANNOT BE TRUE IF activate.pdf.color.mat is FALSE
# parameters for fun.mat.fixed.window.size()
# use exactly the same parameters than for the main matrices (all the .plot parameters). From width.wind.mat.plot to label.amplif.scale.mat.plot


#### parameters for the vizualisation of MDS analysis


# parameters for fun_open_window() and windows opening
activate.pdf.mds <- TRUE # write TRUE for pdf display and FALSE for R display (graphs related to the MWW tests used in the MWW method)
plotted.axe.nb.mds <- 4 # number of CP taken into account to draw the MDS plans (from 2 to number of files in file.name1 and file.name2 - 1. If over this max number, use the max number)
confidence.kind.mds <- "bar" # either "ellipsoid" or "bar" (orthogonal bars). Not used if mds.confidence.int == FALSE
width.wind.mds <- 7 # window width (in inches)
height.wind.mds <- 7 # window height (in inches)
down.space.mds <- 0.5 # bottom margin space (in inches)
left.space.mds <- 0.5 # left margin space (in inches)
up.space.mds <- 1 # top margin space (in inches)
right.space.mds <- 0.5 # right margin space (in inches)
amplif.mds <- 14 # increase or decrease the value to increase or decrease the size of the axis numbers and legend
names.amplif.mds <- 3 # increase or decrease the value to increase or decrease the size of the axis numbers and legend
dot.size.amplif.mds <- 2 # increase or decrease the value to increase or decrease the size of the dots (not for cos2 drawings)
amplif.corner.text.mds <- 0.4 # increase or decrease the corner text


################ Other


optional.text <- "" # write here an optional text to include in results and graphs
warn.secu <- FALSE # logical. Display if internal homemade functions of anova_contrasts and anova_contrasts have variables that are present in other environments?


################ End Optional settings


################################ End Parameters that need to be set by the user




