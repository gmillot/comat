[//]: # "#to make links in gitlab: example with racon https://github.com/isovic/racon"
[//]: # "tricks in markdown: https://openclassrooms.com/fr/courses/1304236-redigez-en-markdown"

| usage | R dependencies |
| --- | --- |
| [![R Version](https://img.shields.io/badge/code-R-blue?style=plastic)](https://cran.r-project.org/mirrors.html) | [![Dependencies: R Version](https://img.shields.io/badge/R-v4.0.2-blue?style=plastic)](https://cran.r-project.org/mirrors.html) |
| [![License: GPL-3.0](https://img.shields.io/badge/licence-GPL%20(%3E%3D3)-green?style=plastic)](https://www.gnu.org/licenses) | [![Dependencies: R Package](https://img.shields.io/badge/package-jpeg%20v0.1_8.1-blue?style=plastic)](https://cran.r-project.org/web/packages/jpeg/index.html) |


## TABLE OF CONTENTS

   - [AIM](#aim)
   - [CONTENT](#content)
   - [HOW TO RUN](#how-to-run)
   - [OUTPUT](#output)
   - [VERSIONS](#versions)
   - [LICENCE](#licence)
   - [CITATION](#citation)
   - [CREDITS](#credits)
   - [ACKNOWLEDGEMENTS](#Acknowledgements)
   - [WHAT'S NEW IN](#what's-new-in)


## AIM

Perform 2 by 2 comparison of contingency matrices of same dimension in batches

Perform matrix quality control, matrix normalization, matrix distances, permutation test, corrected p values, metric multidimensional scale (MMDS) analysis and graphics


## CONTENT

| Files and folder | Description |
| :--- | :--- |
| **comat.R** | File that can be executed using a CLI (command line interface) or sourced in R or RStudio. |
| **comat.config** | Parameter settings for the comat.R file. |
| **other_comat_functions.R** | Additional functions for comat.R. |
| **dataset folder** | Folder containing some datasets than can be used as examples. |
| **example_of_result folder** | Folder containing an example of result obtained with dataset\data4. |
| **Licence.txt** | Licence of the release. |


## HOW TO RUN comat

### Using a R GUI (graphic user interface, i.e., R or RStudio windows)

1) Open the comat.config file and set the parameters. The file must be present in the same directory as comat.R

2) Open R or RStudio

3) Source the comat.R file, for instance using the following instruction:


	`  source("C:/Users/Gael/Desktop/comat.R")  `


### Using a R CLI (command line interface)

1) Open the comat.config file and set the parameters. The file must be present in the same directory as comat.R

2) Open a shell windows

3) run comat.R, for instance using the following instruction:

	`  Rscript comat.R comat.config  `

For cygwin, use something like:

`  /cygdrive/c/Program\ Files/R/R-4.0.2/bin/Rscript comat.R comat.config  `


## comat OUTPUT

| Mandatory Files | Description |
| --- | --- |
| **xxxxxxxxxx_report.txt** | report file |
| **all_objects.RData** | R file containing all the objects created during the run. Open it to inspects the objects |
| **Comat Matrix plotting xxxxxxxxxx.pdf** | heatmap of the compared matrices |
| **Comat MDS graphical analysis xxxxxxxxxx.pdf** | heatmap of distances and MDS plots |
| **Comat VT graphical analysis xxxxxxxxxx.pdf** | empirical distribution of the Variable of Test (VT) and p value |

| Optional Files (parameter readjust different from "NO") | Description |
| --- | --- |
| **equal_proportional_min xxxxxxxxxxx.txt** | modified matrix |




## VERSIONS

The different releases are tagged [here](https://gitlab.pasteur.fr/gmillot/comat/-/tags)


## LICENCE

This package of scripts can be redistributed and/or modified under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
Distributed in the hope that it will be useful, but without any warranty; without even the implied warranty of merchandability or fitness for a particular purpose.
See the GNU General Public License for more details at https://www.gnu.org/licenses or in the Licence.txt attached file.


## CITATION

Not yet published


## CREDITS

[Gael A. Millot](https://gitlab.pasteur.fr/gmillot), Hub-CBD, Institut Pasteur, USR 3756 IP CNRS, Paris, France


## ACKNOWLEDGEMENTS

Vincent Guillemot, Hub-CBD, Institut Pasteur, Paris, France

Herve Abdi, University of Texas, Dallas, USA

R and R packages developers

Gitlab developers


## WHAT'S NEW IN

### v6.2

- .config file saved in the result folder


### v6.1

- Issue 125 solved: now the fixed margins printed in the report file are detailed


### v6.0

- Two parameters added: show_theo and cpu_nb
- Now check that cpu_nb is compatible with available cpu
- Parall debugged


### v5.2

- MDS error message added
- All Gael replaced by gmillot in path


### v5.1

Config file now named comat.rconfig


### v5.0

New parameter empty.rm added


### v4.3.0

1) upgraded to support cute v11.6


### v4.2.0

1) problem of row names of heatmaps solved (due to non rounded quantiles in fun.mat.post.graph.feature)


### v4.1.0

1) some corrections on cbind()


### v4.0.0

1) upgrade to R-4.0.2 


### v3.1.0

1) weighted sampling added to reflect pairing 


### v3.0.0

1) update with cute functions 


### v2.2.0

1) Jpeg matrix added

2) by_case_matrix_operation.doc now in cute_little_R_functions (fun_by_case_matrix_op())

3) Still a beta version


### v2.1.0

1) Kmeans and clustering added in the MMDS graphs

2) Still a beta version


### v2.0.0

1) MMDS added

2) Still a beta version


### v1.0.0

1) Everything
